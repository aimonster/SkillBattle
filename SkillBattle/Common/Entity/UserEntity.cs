﻿using Common.DTO.Account;
using Common.Interfaces.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Common.Helpers;

namespace Common.Entity
{
    public class UserEntity : IUser
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string NickName { get; set; }
         
        public string PhoneNumber { get; set; }

        public DateTime BirthDate { get; set; }

        public DateTime RegistrationDate { get; set; }

        public bool IsEmailConfirmed { get; set; }

        public List<UsersTeamsEntity> UsersTeams { get; set; }

        public UserEntity() { }

        public UserEntity(RegistrationDTO data)
        {
            Email = data.Email;
            NickName = data.NickName;
            PhoneNumber = data.PhoneNumber;
            //BirthDate = data.BirthDate;
            RegistrationDate = DateTime.Now;
            IsEmailConfirmed = false;
            Password = TripleDESCryptHelper.Encript(data.Password);
        }
    }
}
