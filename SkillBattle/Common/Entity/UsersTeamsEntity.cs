﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Common.Entity
{
    public class UsersTeamsEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public UserEntity User { get; set; }
        public int UserId { get; set; }

        public TeamEntity Team { get; set; }
        public int TeamId { get; set; }

        public int KilledPeople { get; set; }
    }
}
