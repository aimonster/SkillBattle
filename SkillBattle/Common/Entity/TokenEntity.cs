﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Entity
{
    public class TokenEntity
    {
        /// <summary>
        /// Gets or Sets the identifier
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or Sets the user
        /// </summary>
        public UserEntity User { get; set; }

        /// <summary>
        /// Gets or Sets the user id
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Gets or Sets the expiration date
        /// </summary>
        public DateTime ExpirationDate { get; set; }
    }
}
