﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Common.Entity
{
    public class TeamEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Name { get; set; }

        public GameStatusTypes GameStatus { get; set; }
        public TeamTypes TeamType { get; set; }

        public MatchEntity Match { get; set; }
        public int MatchId { get; set; }

        public List<UsersTeamsEntity> UsersTeams { get; set; }
    }
}
