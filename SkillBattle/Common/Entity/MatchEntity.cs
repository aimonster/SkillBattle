﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Common.Entity
{
    public class MatchEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Name { get; set; }       

        public List<TeamEntity> Teams { get; set; }

        public bool HasStarted { get; set; }

        public TimeSpan Duration { get; set; }

        
    }
}
