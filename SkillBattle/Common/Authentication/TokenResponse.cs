﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Authentication
{
    public class TokenResponse
    {
        public string Token { get; set; }
        public string Role { get; set; }
        public string UserId { get; set; }
    }
}
