﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Interfaces.Entity
{
    public interface IUser
    {
        int Id { get; set; }
       
        string Email { get; set; }
       
        string NickName { get; set; }
       
        string PhoneNumber { get; set; }
       
        DateTime BirthDate { get; set; }
       
        DateTime RegistrationDate { get; set; }
       
        bool IsEmailConfirmed { get; set; }
    }
}
