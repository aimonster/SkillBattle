﻿using Common.DTO.Account;
using Common.DTO.Communication;
using Common.Enums;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Common.Interfaces.Services
{
    public interface IAccountService
    {
        Task<Response<OperationResults>> Register(RegistrationDTO data, HttpRequest request, CancellationToken token = new CancellationToken());
        Task<Response<UserDTO>> ConfirmEmail(string token);
        Task<Response<UserDTO>> RemoveUser(int id);
    }
}
