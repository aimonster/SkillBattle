﻿using Common.Authentication;
using Common.DTO.Communication;
using Common.Interfaces.Entity;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Common.Interfaces.Services
{
    public interface ILoginService
    {
        Task<IUser> GetUserByCreds(string login, string password);

        Task<Response<ClaimsIdentity>> GetIdentity(string login, string password);

        Task<Response<TokenResponse>> GetToken(ClaimsIdentity identity);

        //Task<bool> IsSocketTokenExist(string token, bool isNeedAdminPremissions);

        //Task<Response<SocketResponse>> GetSocketToken(string login, string password);

        //Task RemoveSocketToken(string token);
    }
}
