﻿using Common.DTO.Communication;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Common.Interfaces.Services
{
    public interface ISendingService
    {
        Task Send(string receiver, string message, string subject, SendMessageTypes type);

        Task Send(List<string> receivers, string message, string subject, SendMessageTypes type);
        
    }
}
