﻿using Common.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTO.Account
{
    public class UserDTO
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }

        public UserDTO() { Role = "user"; }

        public UserDTO(UserEntity entity)
        {
            Login = entity.NickName;
            Role = "user";
            
        }
    }
}
