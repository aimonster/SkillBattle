﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Enums
{
    public enum GameStatusTypes
    {
        Won,
        Lost,
        Draw,
        NotStarted
    }
}
