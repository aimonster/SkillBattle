﻿using Common.Helpers;
using Common.Interfaces.Services;
using DataAccessLayer.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.TimeredFunctionsService
{
    public class TimeredFunctionsService : ITimeredFunctionsService
    {
        private readonly MSContext _db;        
       

        public TimeredFunctionsService(MSContext db)
        {
            _db = db;          
             
        }

        public async Task<bool> Setup()
        {
            var switcher = TimerConfigurator.GetConfiguredTimer(60 * 60, Switcher);
            //var switcher = TimerConfigurator.GetConfiguredTimer(20, Switcher);
            switcher.Start();

            return true;
        }

        private async Task Switcher()
        {
            var now = DateTime.UtcNow;

            if (now.Day == 1 && now.Hour < 1)
            {
                //await SendMonthlyReport();
            }

            if (now.Hour < 1)
            {
                //await ParseConferencesFromDou();
                //await RemoveOldConferences();
                await CheckEndsTokenExpirates();

            }
            //await CheckPollStarting();
            //await CheckEndsPetitionVoting();
        }

        public async Task CheckEndsTokenExpirates()
        {
            var now = DateTime.UtcNow;
            var oldTokens = await _db.Tokens.Where(p => p.ExpirationDate < now).ToListAsync();
            _db.Tokens.RemoveRange(oldTokens);
            await _db.SaveChangesAsync();
        }

    }
}
