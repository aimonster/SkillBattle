﻿using Common.DTO.Communication;
using Common.Enums;
using Common.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.SendingService
{
    public class SendingService : ISendingService
    {
        //private readonly ISmsService _smsService;
        private readonly IEmailService _emailService;        

        public SendingService(IEmailService emailService)
        {
            //_smsService = smsService;
            _emailService = emailService;            
        }

        public async Task Send(string receiver, string message, string subject, SendMessageTypes type)
        {
            await Send(new List<string> { receiver }, message, subject, type);
        }

        public async Task Send(List<string> receivers, string message, string subject, SendMessageTypes type)
        {
            switch (type)
            {
                case SendMessageTypes.Email:
                    await _emailService.SendMail(receivers, message, subject);
                    break;
                case SendMessageTypes.Sms:
                    //await _smsService.SendSms(receivers, message);
                    break;                
                default:
                    throw new NotImplementedException();
            }
        }

        
    }
}
