﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.LoginService
{
    public class AuthOptions
    {
        public const string ISSUER = "skill_battle_web_api_server";
        public const string AUDIENCE = "skill_battle_web_api_users";
        const string KEY = "security_80_lvl_Magnis_the_best";
        public const int LIFETIME = 60 * 24;
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
